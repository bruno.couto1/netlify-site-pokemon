import React , { createContext, useState } from 'react';
import './App.css';
import { Router } from '@reach/router';
import  Login from './Paginas/Login/Login';
import Home from './Paginas/Home/Home';//depois, pra usar a home certa, muda essa rota aqui
import Perfil from './Paginas/Perfil/Perfil';
import Dados_Pokemon_test from './Paginas/Dados_pokemon/Dados_pokemon';
import Footer from './Paginas/Footer/Footer';
export const myContext = createContext();

function App() {

  const [value, setValue] = useState({
    user:'',
    pokemon:''
  })

  const Dados_Pokemon = () => <h1>Pagina dos dados do pokemon</h1>

  const NotFound = () => <h1>404</h1>

  return (
    <myContext.Provider value={ {value , setValue }}>
      <Router>
        <Login path="/"/>
        <Home path="home"/>
        <Perfil path="perfil"/>
        <Dados_Pokemon_test path="dados-pokemon"/>
        <NotFound default/>
      </Router>
      <Footer/>
    </myContext.Provider>
  );
}

export default App;
