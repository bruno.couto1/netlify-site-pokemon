import React from 'react';
import "./Header.css";
import { Link } from '@reach/router';

const Header = () => (
 <header id='main-header'>
    <span>Pokedex</span>
    <div >
        <Link className= 'botao-link' to='../home' className="header_link">Home</Link>
        <Link className= 'botao-link' to='../perfil' className="header_link">Perfil</Link>
    </div>
 </header>
)
export default Header;