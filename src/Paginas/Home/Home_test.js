import React from 'react';
import axios from 'axios';
import { myContext } from '../../App'



function Home (){
    const {value, setValue } = React.useContext(myContext);
    const [poke, setPoke] = React.useState();
    const [userData, setUserData] = React.useState();
    
    React.useEffect ( () => {//carrega os dados do usuário e coloca em userData
        axios.get( (" https://pokedex20201.herokuapp.com/users/" + value.user))
        .then((e) => {
            console.log(e.data)
            setUserData(e.data); 
        })
        return;
    },[])

    React.useEffect ( () => {//carrega os dados do pokemon e coloca em poke
        axios.get( (" https://pokedex20201.herokuapp.com/pokemons"))
        .then((e) => {
            console.log(e.data.data)
            setPoke(e.data.data)
        })
        return;
    },[])
    
    const adicionaPokemon = async (pokename, username) =>{
        if(userData){
            
            if(userData.pokemons.some((e) => e.name === pokename)){
                //ignora
                console.log("ERRO: item " + pokename + " já existe na lista de favoritos")
            }
            else {//adiciona
                await axios.post( (" https://pokedex20201.herokuapp.com/users/" + username + '/starred/' + pokename))
            }
        }
    }


    return(
    <>
        <h1>O pokemon é {value && value.pokemon}</h1>
        <div>
            {poke && poke.map( (data) => (
                <>
                <h3>{data.name}</h3>
                <img 
                    src={data.image_url} 
                    alt="imagem do pokemon" 
                />
                <br/>
                <button onClick = { (e) => adicionaPokemon( data.name , value.user) }>Adiciona</button>
                {/*<button onClick = { (e) => retiraPokemon( data.name , value.user) }>Desfavoritar</button> */}
                </>
            ))
            }
        </div>
    </>
    )
};
export default Home;
