import React, { useState, useContext, useRef} from 'react';
import axios from 'axios';
import { navigate } from "@reach/router";
import { myContext } from '../../App'
import './Login.css'
// quando eu dou submit no nome, o usuário é direcionado pra página de login dele.
//Tem que mudar isso depois
function Login () {
    const {value, setValue } = useContext(myContext);
    const username = React.useRef('');
    const [flag, setFlag] = useState(true); //usado para saber se o usuário inserido está válido

    const validate = async (nome_inserido) => {
        let flag_rapida = true
        await axios
            .post("https://pokedex20201.herokuapp.com/users",{
            username: nome_inserido
            })
            .then((e) => {
                setFlag (true);
                flag_rapida = true;
            })
            .catch(error => {
                setFlag (false);
                flag_rapida = false;
            });
            return flag_rapida;
        }
        
        const handleSubmit = async (event) => {
            if(username.current.value.trim() == '') return;
            event.preventDefault();
                if( await validate(username.current.value) ) {
                    setValue({
                        user: username.current.value,
                        pokemon: value.pokemon
                    })
                    navigate('home') //Muda para a página do usuário
                } 
            }
            
            const mostraErro = () => {
                if( flag === false ){
                    return(
                        <div className='mensagemError'>
                            <h4>
                                O nome de usuário já foi escolhido. Deseja entrar com esse usuário ou criar outro?
                            </h4>
                            <button className='botao' onClick={()=> {
                                setValue({
                                    user: username.current.value,
                                    pokemon: value.pokemon
                                })
                                navigate('home');
                            }}> Entrar </button> 
                            <button className='botao' onClick={(e) => {
                                username.current.value = ''
                            }}>Criar Outro</button>
                        </div>
            )
        }
    }


 return(
    <>
    <body>
        <div className='login-container'>
            <h1>Pokedex</h1>
            <h3>Digite o nome do novo treinador</h3>
            <form onSubmit={(e) => handleSubmit(e)}>
                <input ref={username} name='name' type='text' />
                <button className='botao' type="submit">Enviar</button>
                {mostraErro()}
            </form>
        </div>
    </body>
    </>
 )   
};
export default Login;